package net.myek.narutosmp.procedures;

import net.myek.narutosmp.NarutosmpMod;

import net.minecraft.world.IWorld;
import net.minecraft.particles.ParticleTypes;

import java.util.Map;

public class SmokeBombeffectProcedure {
	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("x") == null) {
			if (!dependencies.containsKey("x"))
				NarutosmpMod.LOGGER.warn("Failed to load dependency x for procedure SmokeBombeffect!");
			return;
		}
		if (dependencies.get("y") == null) {
			if (!dependencies.containsKey("y"))
				NarutosmpMod.LOGGER.warn("Failed to load dependency y for procedure SmokeBombeffect!");
			return;
		}
		if (dependencies.get("z") == null) {
			if (!dependencies.containsKey("z"))
				NarutosmpMod.LOGGER.warn("Failed to load dependency z for procedure SmokeBombeffect!");
			return;
		}
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				NarutosmpMod.LOGGER.warn("Failed to load dependency world for procedure SmokeBombeffect!");
			return;
		}
		double x = dependencies.get("x") instanceof Integer ? (int) dependencies.get("x") : (double) dependencies.get("x");
		double y = dependencies.get("y") instanceof Integer ? (int) dependencies.get("y") : (double) dependencies.get("y");
		double z = dependencies.get("z") instanceof Integer ? (int) dependencies.get("z") : (double) dependencies.get("z");
		IWorld world = (IWorld) dependencies.get("world");
		double xRadius = 0;
		double loop = 0;
		double zRadius = 0;
		double particleAmount = 0;
		loop = (double) 10;
		particleAmount = (double) 32;
		xRadius = (double) 5;
		zRadius = (double) 5;
		while ((loop < particleAmount)) {
			world.addParticle(ParticleTypes.LARGE_SMOKE, ((x + 0.5) + (Math.cos((((Math.PI * 2) / particleAmount) * loop)) * xRadius)), y,
					((z + 0.5) + (Math.sin((((Math.PI * 2) / particleAmount) * loop)) * zRadius)), 0, 0.05, 0);
			loop = (double) (loop + 1);
		}
	}
}
