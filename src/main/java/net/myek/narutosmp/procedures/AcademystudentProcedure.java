package net.myek.narutosmp.procedures;

import net.myek.narutosmp.NarutosmpModVariables;
import net.myek.narutosmp.NarutosmpMod;

import net.minecraft.entity.Entity;

import java.util.Map;

public class AcademystudentProcedure {
	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				NarutosmpMod.LOGGER.warn("Failed to load dependency entity for procedure Academystudent!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		if ((((entity.getCapability(NarutosmpModVariables.PLAYER_VARIABLES_CAPABILITY, null)
				.orElse(new NarutosmpModVariables.PlayerVariables())).Rank_academyStudent) == (true))) {
			System.out.println("hallo");
		}
	}
}
