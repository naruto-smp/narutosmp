package net.myek.narutosmp.procedures;

import net.myek.narutosmp.NarutosmpMod;

import net.minecraft.world.IWorld;
import net.minecraft.particles.ParticleTypes;

import java.util.Map;

public class SmokebombeffecttestProcedure {
	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("world") == null) {
			if (!dependencies.containsKey("world"))
				NarutosmpMod.LOGGER.warn("Failed to load dependency world for procedure Smokebombeffecttest!");
			return;
		}
		IWorld world = (IWorld) dependencies.get("world");
		double xRadius = 0;
		double loop = 0;
		double zRadius = 0;
		double particleAmount = 0;
		double loopamount = 0;
		double lowestY = 0;
		double highestY = 0;
		lowestY = (double) (-3);
		highestY = (double) 3;
		xRadius = (double) 1;
		zRadius = (double) 1;
		particleAmount = (double) 8;
		loopamount = (double) 5;
		while (((world.isRemote()) == (true))) {
			for (int index1 = 0; index1 < (int) (loopamount); index1++) {
				world.addParticle(ParticleTypes.EXPLOSION, (xRadius + 5), lowestY, zRadius, 0, 1, 0);
			}
		}
	}
}
