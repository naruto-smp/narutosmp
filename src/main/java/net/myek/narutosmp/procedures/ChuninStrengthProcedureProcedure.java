package net.myek.narutosmp.procedures;

import net.myek.narutosmp.potion.ChuninstrengthPotionEffect;
import net.myek.narutosmp.NarutosmpModVariables;
import net.myek.narutosmp.NarutosmpMod;

import net.minecraft.potion.EffectInstance;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Entity;

import java.util.Map;

public class ChuninStrengthProcedureProcedure {
	public static void executeProcedure(Map<String, Object> dependencies) {
		if (dependencies.get("entity") == null) {
			if (!dependencies.containsKey("entity"))
				NarutosmpMod.LOGGER.warn("Failed to load dependency entity for procedure ChuninStrengthProcedure!");
			return;
		}
		if (dependencies.get("sourceentity") == null) {
			if (!dependencies.containsKey("sourceentity"))
				NarutosmpMod.LOGGER.warn("Failed to load dependency sourceentity for procedure ChuninStrengthProcedure!");
			return;
		}
		Entity entity = (Entity) dependencies.get("entity");
		Entity sourceentity = (Entity) dependencies.get("sourceentity");
		if (((((entity.getCapability(NarutosmpModVariables.PLAYER_VARIABLES_CAPABILITY, null)
				.orElse(new NarutosmpModVariables.PlayerVariables())).Rank_chunin) == (true)) && (sourceentity instanceof PlayerEntity))) {
			if (entity instanceof LivingEntity)
				((LivingEntity) entity).addPotionEffect(new EffectInstance(ChuninstrengthPotionEffect.potion, (int) 1e+142, (int) 1));
		}
	}
}
