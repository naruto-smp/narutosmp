package net.myek.narutosmp;

import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.fml.network.NetworkEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.common.util.FakePlayer;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.Capability;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Direction;
import net.minecraft.network.PacketBuffer;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.Entity;
import net.minecraft.client.Minecraft;

import java.util.function.Supplier;

public class NarutosmpModVariables {
	public NarutosmpModVariables(NarutosmpModElements elements) {
		elements.addNetworkMessage(PlayerVariablesSyncMessage.class, PlayerVariablesSyncMessage::buffer, PlayerVariablesSyncMessage::new,
				PlayerVariablesSyncMessage::handler);
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::init);
	}

	private void init(FMLCommonSetupEvent event) {
		CapabilityManager.INSTANCE.register(PlayerVariables.class, new PlayerVariablesStorage(), PlayerVariables::new);
	}
	@CapabilityInject(PlayerVariables.class)
	public static Capability<PlayerVariables> PLAYER_VARIABLES_CAPABILITY = null;
	@SubscribeEvent
	public void onAttachCapabilities(AttachCapabilitiesEvent<Entity> event) {
		if (event.getObject() instanceof PlayerEntity && !(event.getObject() instanceof FakePlayer))
			event.addCapability(new ResourceLocation("narutosmp", "player_variables"), new PlayerVariablesProvider());
	}
	private static class PlayerVariablesProvider implements ICapabilitySerializable<INBT> {
		private final LazyOptional<PlayerVariables> instance = LazyOptional.of(PLAYER_VARIABLES_CAPABILITY::getDefaultInstance);
		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
			return cap == PLAYER_VARIABLES_CAPABILITY ? instance.cast() : LazyOptional.empty();
		}

		@Override
		public INBT serializeNBT() {
			return PLAYER_VARIABLES_CAPABILITY.getStorage().writeNBT(PLAYER_VARIABLES_CAPABILITY, this.instance.orElseThrow(RuntimeException::new),
					null);
		}

		@Override
		public void deserializeNBT(INBT nbt) {
			PLAYER_VARIABLES_CAPABILITY.getStorage().readNBT(PLAYER_VARIABLES_CAPABILITY, this.instance.orElseThrow(RuntimeException::new), null,
					nbt);
		}
	}

	private static class PlayerVariablesStorage implements Capability.IStorage<PlayerVariables> {
		@Override
		public INBT writeNBT(Capability<PlayerVariables> capability, PlayerVariables instance, Direction side) {
			CompoundNBT nbt = new CompoundNBT();
			nbt.putBoolean("MainQuestDone2", instance.MainQuestDone2);
			nbt.putBoolean("MainQuestDone1", instance.MainQuestDone1);
			nbt.putBoolean("MainQuestDone3", instance.MainQuestDone3);
			nbt.putBoolean("MainQuestDone4", instance.MainQuestDone4);
			nbt.putBoolean("MainQuestDone5", instance.MainQuestDone5);
			nbt.putBoolean("Rank_academyStudent", instance.Rank_academyStudent);
			nbt.putBoolean("Rank_genin", instance.Rank_genin);
			nbt.putBoolean("Rank_chunin", instance.Rank_chunin);
			nbt.putBoolean("Rank_jounin", instance.Rank_jounin);
			nbt.putBoolean("Rank_SpecialJounin", instance.Rank_SpecialJounin);
			nbt.putBoolean("Rank_Anbu", instance.Rank_Anbu);
			nbt.putBoolean("Rank_Sage", instance.Rank_Sage);
			nbt.putBoolean("Rank_Kage", instance.Rank_Kage);
			return nbt;
		}

		@Override
		public void readNBT(Capability<PlayerVariables> capability, PlayerVariables instance, Direction side, INBT inbt) {
			CompoundNBT nbt = (CompoundNBT) inbt;
			instance.MainQuestDone2 = nbt.getBoolean("MainQuestDone2");
			instance.MainQuestDone1 = nbt.getBoolean("MainQuestDone1");
			instance.MainQuestDone3 = nbt.getBoolean("MainQuestDone3");
			instance.MainQuestDone4 = nbt.getBoolean("MainQuestDone4");
			instance.MainQuestDone5 = nbt.getBoolean("MainQuestDone5");
			instance.Rank_academyStudent = nbt.getBoolean("Rank_academyStudent");
			instance.Rank_genin = nbt.getBoolean("Rank_genin");
			instance.Rank_chunin = nbt.getBoolean("Rank_chunin");
			instance.Rank_jounin = nbt.getBoolean("Rank_jounin");
			instance.Rank_SpecialJounin = nbt.getBoolean("Rank_SpecialJounin");
			instance.Rank_Anbu = nbt.getBoolean("Rank_Anbu");
			instance.Rank_Sage = nbt.getBoolean("Rank_Sage");
			instance.Rank_Kage = nbt.getBoolean("Rank_Kage");
		}
	}

	public static class PlayerVariables {
		public boolean MainQuestDone2 = false;
		public boolean MainQuestDone1 = false;
		public boolean MainQuestDone3 = false;
		public boolean MainQuestDone4 = false;
		public boolean MainQuestDone5 = false;
		public boolean Rank_academyStudent = true;
		public boolean Rank_genin = false;
		public boolean Rank_chunin = false;
		public boolean Rank_jounin = false;
		public boolean Rank_SpecialJounin = false;
		public boolean Rank_Anbu = false;
		public boolean Rank_Sage = false;
		public boolean Rank_Kage = false;
		public void syncPlayerVariables(Entity entity) {
			if (entity instanceof ServerPlayerEntity)
				NarutosmpMod.PACKET_HANDLER.send(PacketDistributor.PLAYER.with(() -> (ServerPlayerEntity) entity),
						new PlayerVariablesSyncMessage(this));
		}
	}
	@SubscribeEvent
	public void onPlayerLoggedInSyncPlayerVariables(PlayerEvent.PlayerLoggedInEvent event) {
		if (!event.getPlayer().world.isRemote())
			((PlayerVariables) event.getPlayer().getCapability(PLAYER_VARIABLES_CAPABILITY, null).orElse(new PlayerVariables()))
					.syncPlayerVariables(event.getPlayer());
	}

	@SubscribeEvent
	public void onPlayerRespawnedSyncPlayerVariables(PlayerEvent.PlayerRespawnEvent event) {
		if (!event.getPlayer().world.isRemote())
			((PlayerVariables) event.getPlayer().getCapability(PLAYER_VARIABLES_CAPABILITY, null).orElse(new PlayerVariables()))
					.syncPlayerVariables(event.getPlayer());
	}

	@SubscribeEvent
	public void onPlayerChangedDimensionSyncPlayerVariables(PlayerEvent.PlayerChangedDimensionEvent event) {
		if (!event.getPlayer().world.isRemote())
			((PlayerVariables) event.getPlayer().getCapability(PLAYER_VARIABLES_CAPABILITY, null).orElse(new PlayerVariables()))
					.syncPlayerVariables(event.getPlayer());
	}

	@SubscribeEvent
	public void clonePlayer(PlayerEvent.Clone event) {
		PlayerVariables original = ((PlayerVariables) event.getOriginal().getCapability(PLAYER_VARIABLES_CAPABILITY, null)
				.orElse(new PlayerVariables()));
		PlayerVariables clone = ((PlayerVariables) event.getEntity().getCapability(PLAYER_VARIABLES_CAPABILITY, null).orElse(new PlayerVariables()));
		clone.MainQuestDone2 = original.MainQuestDone2;
		clone.MainQuestDone1 = original.MainQuestDone1;
		clone.MainQuestDone3 = original.MainQuestDone3;
		clone.MainQuestDone4 = original.MainQuestDone4;
		clone.MainQuestDone5 = original.MainQuestDone5;
		clone.Rank_academyStudent = original.Rank_academyStudent;
		clone.Rank_genin = original.Rank_genin;
		clone.Rank_chunin = original.Rank_chunin;
		clone.Rank_jounin = original.Rank_jounin;
		clone.Rank_SpecialJounin = original.Rank_SpecialJounin;
		clone.Rank_Anbu = original.Rank_Anbu;
		clone.Rank_Sage = original.Rank_Sage;
		clone.Rank_Kage = original.Rank_Kage;
		if (!event.isWasDeath()) {
		}
	}
	public static class PlayerVariablesSyncMessage {
		public PlayerVariables data;
		public PlayerVariablesSyncMessage(PacketBuffer buffer) {
			this.data = new PlayerVariables();
			new PlayerVariablesStorage().readNBT(null, this.data, null, buffer.readCompoundTag());
		}

		public PlayerVariablesSyncMessage(PlayerVariables data) {
			this.data = data;
		}

		public static void buffer(PlayerVariablesSyncMessage message, PacketBuffer buffer) {
			buffer.writeCompoundTag((CompoundNBT) new PlayerVariablesStorage().writeNBT(null, message.data, null));
		}

		public static void handler(PlayerVariablesSyncMessage message, Supplier<NetworkEvent.Context> contextSupplier) {
			NetworkEvent.Context context = contextSupplier.get();
			context.enqueueWork(() -> {
				if (!context.getDirection().getReceptionSide().isServer()) {
					PlayerVariables variables = ((PlayerVariables) Minecraft.getInstance().player.getCapability(PLAYER_VARIABLES_CAPABILITY, null)
							.orElse(new PlayerVariables()));
					variables.MainQuestDone2 = message.data.MainQuestDone2;
					variables.MainQuestDone1 = message.data.MainQuestDone1;
					variables.MainQuestDone3 = message.data.MainQuestDone3;
					variables.MainQuestDone4 = message.data.MainQuestDone4;
					variables.MainQuestDone5 = message.data.MainQuestDone5;
					variables.Rank_academyStudent = message.data.Rank_academyStudent;
					variables.Rank_genin = message.data.Rank_genin;
					variables.Rank_chunin = message.data.Rank_chunin;
					variables.Rank_jounin = message.data.Rank_jounin;
					variables.Rank_SpecialJounin = message.data.Rank_SpecialJounin;
					variables.Rank_Anbu = message.data.Rank_Anbu;
					variables.Rank_Sage = message.data.Rank_Sage;
					variables.Rank_Kage = message.data.Rank_Kage;
				}
			});
			context.setPacketHandled(true);
		}
	}
}
